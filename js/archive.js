(function (Drupal, drupalSettings) {
  'use strict';

  let form = document.querySelector("form");

  const messages = new Drupal.Message();
  function clearMessages() {
    // Clear message that does not have wrapper.
    let messageWithoutWrapper = document.querySelector("[data-drupal-messages]>div:not(.messages__wrapper)");
    if (messageWithoutWrapper) {
      messageWithoutWrapper.remove();
    }

    messages.clear();
  }

  let settings = drupalSettings.zipload.settings;

  function throbberStop () {
    document.getElementsByClassName("ajax-progress")[0].remove();
  }

  function throbberStart(node, message) {
    let throbber = Drupal.theme.ajaxProgressThrobber(message);
    node.parentNode.insertAdjacentHTML('beforeend', throbber);
  }

  function bytesToSize(bytes) {
    let sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
  }

  form.addEventListener('submit', function (event) {
    let files = this['files[files][]'].files;
    let destination = this['destination'].value;
    let size = Array.from(files).reduce(function (t, f) {
      return t + f.size;
    }, 0);
    if (size > settings.maxUploadSize && !this['zip'].checked) {
      event.preventDefault();
      clearMessages();
      messages.add(Drupal.t("The total @s size of files exceeds the server's @fmus max limit.<br>Consider to check the compressed option bellow.", {"@s": bytesToSize(size), "@fmus": settings.formattedMaxUploadSize}), {type: 'error'});
    }

    if (this['zip'].checked) {
      event.preventDefault();
    }
    else {
      return;
    }

    clearMessages();
    let validation = true;
    if (!files.length) {
      messages.add(Drupal.t('Please choose at least one file to upload.'), {type: 'error'});
      validation = false;
    }
    if (destination === '') {
      messages.add(Drupal.t('Please specify the upload directory.'), {type: 'error'});
      validation = false;
    }
    if (!validation) {
      return;
    }

    let submit = this['op'];
    throbberStart(submit, Drupal.t('Compressing...'));

    let zip = JSZip();
    for (let i = 0; i < files.length; i++) {
      let f = files[i];
      zip.file(f.name, f);
    }
    zip.generateAsync({type:"blob", compression: "DEFLATE"}).then(function(content) {
      throbberStop();
      throbberStart(submit, Drupal.t('Uploading...'));
      fetch(settings.callback, {
        method: 'POST',
        headers: {
          "Content-Type": content.type,
          "destination": destination
        },
        body: content
      }).then(function(response) {
        throbberStop();
        messages.clear();
        if (response.status == 200) {
          messages.add(Drupal.t(
              'The files have been successfully compressed and uploaded,<br>then decompressed at @dest',
              {'@dest': destination}
          ));
        }
        else {
          response.json().then(function (message) {
            messages.add(
                response.status + " " + response.statusText + ": " + message,
                {type: "error"}
            );
          });
        }
      });
    });
  });
})(Drupal, drupalSettings);
