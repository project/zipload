<?php

/**
 * @file
 * Hooks for the zipload module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter or validate the compressed upload data or the upload destination path.
 *
 * (Optionally subscribe to ZiploadEvent::ZIPLOAD event.)
 *
 * @param string $data
 *   The uploaded compressed files data.
 * @param string $destination
 *   The upload directory path.
 *
 * @throws \Drupal\zipload\Exception\FilesException
 * @throws \Drupal\zipload\Exception\DestinationException
 */
function hook_compressed_upload_alter(&$data, &$destination) {
  $fileSystem = \Drupal::service("file_system");
  $path = $fileSystem->getTempDirectory() . DIRECTORY_SEPARATOR . "test.zip";
  $exclude = ["pdf", "exe", "docx"];
  $zip = new \ZipArchive();
  if (file_put_contents($path, $data) && $zip->open($path) === TRUE) {
    for($i = 0; $i < $zip->numFiles; $i++) {
      $filename = $zip->getNameIndex($i);
      $fileinfo = pathinfo($filename);
      if (in_array($fileinfo['extension'], $exclude)) {
        $zip->close();
        unlink($path);
        throw new \Drupal\zipload\Exception\FilesException(implode(", ", $exclude) . " files are not allowed", 403);
      }
    }
    $zip->close();
    unlink($path);
  }

  if (strpos($destination, "../") === 0) {
    throw new \Drupal\zipload\Exception\DestinationException("Relative paths are not allowed", 403);
  }
}

/**
 * Alter or validate the upload files or the upload destination path.
 *
 * (Optionally subscribe to UploadEvent::UPLOAD event.)
 *
 * @param \Symfony\Component\HttpFoundation\File\UploadedFile[] $files
 *   The uploaded files.
 * @param string $destination
 *   The upload directory path.
 *
 * @throws \Drupal\zipload\Exception\FilesException
 * @throws \Drupal\zipload\Exception\DestinationException
 */
function hook_uncompressed_upload_alter(array &$files, &$destination) {
  foreach ($files as $file) {
    if ($file->getMimeType() === "application/pdf") {
      throw new \Drupal\zipload\Exception\FilesException("pdf files are not allowed", 403);
    }
  }

  if (strpos($destination, "../") === 0) {
    throw new \Drupal\zipload\Exception\DestinationException("Relative paths are not allowed", 403);
  }
}

/**
 * @} End of "addtogroup hooks".
 */
