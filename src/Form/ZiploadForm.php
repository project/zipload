<?php

namespace Drupal\zipload\Form;

use Drupal\Component\Utility\Environment;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\zipload\Event\UploadEvent;
use Drupal\zipload\Exception\DestinationException;
use Drupal\zipload\Exception\FilesException;
use Drupal\zipload\ZiploadService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The file compressing/decompressing upload form.
 */
class ZiploadForm extends FormBase {

  /**
   * The zip upload service.
   *
   * @var \Drupal\zipload\ZiploadService
   */
  protected $service;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $dispatcher;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a form object.
   *
   * @param \Drupal\zipload\ZiploadService $service
   *   The zip upload service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   The event dispatcher service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(ZiploadService $service, EventDispatcherInterface $dispatcher, ModuleHandlerInterface $module_handler) {
    $this->service = $service;
    $this->dispatcher = $dispatcher;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('zipload.service'),
      $container->get('event_dispatcher'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zipload_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form["#attached"]["library"][] = "zipload/jszip";
    $size = Environment::getUploadMaxSize();
    $formatted_size = format_size($size);
    $form["#attached"]["drupalSettings"]["zipload"]["settings"] = [
      "callback" => Url::fromRoute("zipload.callback", [], ["absolute" => TRUE])->toString(),
      "maxUploadSize" => $size,
      "formattedMaxUploadSize" => $formatted_size,
    ];

    $form["destination"] = [
      "#type" => "textfield",
      "#title" => $this->t("Upload directory"),
      "#default_value" => "",
      "#description" => $this->t("Choose an upload directory path.<br>Relative paths to drupal root directory ../<br> public:// or private:// directory paths are equally allowed."),
    ];
    $form["files"] = [
      "#type" => "file",
      "#title" => $this->t("Upload Files"),
      "#multiple" => TRUE,
      "#default_value" => NULL,
      "#description" => $this->t("Choose multiple files to upload.<br>If the total upload size exceeds server's @size max limit consider to check compressed option bellow.", ["@size" => $formatted_size]),
    ];
    $form["zip"] = [
      "#type" => "checkbox",
      "#title" => $this->t("Compress"),
      "#description" => $this->t("Will compress the files before upload if checked<br>After the upload the files will be decompressed again and the zip file will be deleted."),
      "#default_value" => FALSE,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Upload'),
      '#button_type' => 'primary',
    ];

    // By default, render the form using system-config-form.html.twig.
    $form['#theme'] = 'system_config_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $destination = $form_state->getValue("destination");
    $files = $this->getRequest()->files->get("files", []);
    try {
      // Invoke hook_uncompressed_upload_alter()
      // to let modules to alter the validation.
      $this->moduleHandler->alter("uncompressed_upload", $files['files'], $destination);

      // Dispatch an event to allow other modules to subscribe and
      // change the validation by throwing various exceptions.
      $event = new UploadEvent($destination, $files['files']);
      $this->dispatcher->dispatch($event, UploadEvent::UPLOAD);
    }
    catch (DestinationException $e) {
      $form_state->setError($form["destination"], $e->getMessage());
    }
    catch (FilesException $e) {
      $form_state->setError($form["files"], $e->getMessage());
    }
    catch (\Exception $e) {
      $form_state->setError($form, $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $destination = $form_state->getValue("destination");
    /** @var \Symfony\Component\HttpFoundation\File\UploadedFile[] $files */
    $files = $this->getRequest()->files->get("files", [])["files"];
    try {
      $this->service->setUploadDirectory($destination);
      foreach ($files as $file) {
        $file->move($this->service->getUploadDirectory(), $file->getClientOriginalName());
      }

      $this->messenger()->addStatus($this->t("The files have been successfully uploaded to @dest", ['@dest' => $destination]));
    }
    catch (\Exception $exception) {
      $this->messenger()->addError($exception->getMessage());
    }
  }

}
