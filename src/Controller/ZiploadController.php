<?php

namespace Drupal\zipload\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\zipload\Event\ZiploadEvent;
use Drupal\zipload\ZiploadService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides a compressed upload ajax callback.
 */
class ZiploadController extends ControllerBase {

  /**
   * The zip upload service.
   *
   * @var \Drupal\zipload\ZiploadService
   */
  protected $service;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $dispatcher;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The controller constructor.
   *
   * @param \Drupal\zipload\ZiploadService $service
   *   The zip upload service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   The event dispatcher service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(ZiploadService $service, EventDispatcherInterface $dispatcher, ModuleHandlerInterface $module_handler) {
    $this->service = $service;
    $this->dispatcher = $dispatcher;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('zipload.service'),
      $container->get('event_dispatcher'),
      $container->get('module_handler')
    );
  }

  /**
   * Ajax callback for compressed upload.
   */
  public function callback(Request $request) {
    $content = $request->getContent();
    $mimeType = $request->headers->get("Content-Type");
    $destination = $request->headers->get("destination");
    if ($mimeType != "application/zip") {
      return new JsonResponse($this->t("Upload failed because of wrong compression"), 400);
    }

    try {
      // Invoke hook_compressed_upload_alter()
      // to let modules to alter the validation.
      $this->moduleHandler->alter("compressed_upload", $content, $destination);

      // Dispatch an event to allow other modules to subscribe and
      // change the validation by throwing various exceptions.
      $event = new ZiploadEvent($destination, $content);
      $this->dispatcher->dispatch($event, ZiploadEvent::ZIPLOAD);

      $zip = $this->service->setUploadDirectory($destination)->save($content);
      $this->service->extract($zip);
    }
    catch (\Exception $e) {
      return new JsonResponse($e->getMessage(), $e->getCode() ?? 500);
    }

    return new JsonResponse("Success", 200);
  }

}
