<?php

namespace Drupal\zipload\Exception;

/**
 * Exception to be thrown when the value of upload destination is invalid.
 */
class DestinationException extends \Exception {

}
