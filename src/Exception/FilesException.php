<?php

namespace Drupal\zipload\Exception;

/**
 * Exception to be thrown when the uploaded files are invalid.
 */
class FilesException extends \Exception {

}
