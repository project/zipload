<?php

namespace Drupal\zipload\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * The compressed upload event.
 */
class ZiploadEvent extends Event {

  const ZIPLOAD = 'zipload.compressed_upload';

  /**
   * The upload directory path.
   *
   * @var string
   */
  protected $destination;

  /**
   * The uploaded compressed files data.
   *
   * @var string
   */
  protected $data;

  /**
   * ZiploadEvent constructor.
   *
   * @param string $destination
   *   The upload directory path.
   * @param string $data
   *   The uploaded compressed files data.
   */
  public function __construct($destination, $data) {
    $this->destination = $destination;
    $this->data = $data;
  }

  /**
   * Get upload destination.
   *
   * @return string
   *   The upload directory path.
   */
  public function getDestination() {
    return $this->destination;
  }

  /**
   * Set upload destination.
   *
   * @param string $destination
   *   The upload directory path.
   */
  public function setDestination($destination) {
    $this->destination = $destination;
  }

  /**
   * Get compressed files data.
   *
   * @return string
   *   The uploaded compressed files data.
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Set compressed files data.
   *
   * @param string $data
   *   The uploaded compressed files data.
   */
  public function setData($data) {
    $this->data = $data;
  }

}
