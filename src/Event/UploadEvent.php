<?php

namespace Drupal\zipload\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * The upload event.
 */
class UploadEvent extends Event {

  const UPLOAD = 'zipload.uncompressed_upload';

  /**
   * The upload directory path.
   *
   * @var string
   */
  protected $destination;

  /**
   * The uploaded files.
   *
   * @var \Symfony\Component\HttpFoundation\File\UploadedFile[]
   */
  protected $files;

  /**
   * UploadEvent constructor.
   *
   * @param string $destination
   *   The upload directory path.
   * @param \Symfony\Component\HttpFoundation\File\UploadedFile[] $files
   *   The uploaded files.
   */
  public function __construct($destination, array $files) {
    $this->destination = $destination;
    $this->files = $files;
  }

  /**
   * Get upload destination.
   *
   * @return string
   *   The upload directory path.
   */
  public function getDestination() {
    return $this->destination;
  }

  /**
   * Set upload destination.
   *
   * @param string $destination
   *   The upload directory path.
   */
  public function setDestination($destination) {
    $this->destination = $destination;
  }

  /**
   * Get files.
   *
   * @return \Symfony\Component\HttpFoundation\File\UploadedFile[]
   *   The uploaded files.
   */
  public function getFiles() {
    return $this->files;
  }

  /**
   * Set files.
   *
   * @param \Symfony\Component\HttpFoundation\File\UploadedFile[] $files
   *   The uploaded files.
   */
  public function setFiles(array $files) {
    $this->files = $files;
  }

}
