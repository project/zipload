<?php

namespace Drupal\zipload\EventSubscriber;

use Drupal\zipload\Event\UploadEvent;
use Drupal\zipload\Event\ZiploadEvent;
use Drupal\zipload\Exception\DestinationException;
use Drupal\zipload\Exception\FilesException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Uncompressed/compressed upload events subscriber.
 */
class UploadSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      UploadEvent::UPLOAD => ['onUncompressedUpload'],
      ZiploadEvent::ZIPLOAD => ['onCompressedUpload'],
    ];
  }

  /**
   * Uncompressed upload event handler.
   *
   * @param \Drupal\zipload\Event\UploadEvent $event
   *   Uncompressed upload event.
   *
   * @throws \Drupal\zipload\Exception\DestinationException
   * @throws \Drupal\zipload\Exception\FilesException
   */
  public function onUncompressedUpload(UploadEvent $event) {
    if (empty($event->getDestination())) {
      throw new DestinationException("Please specify the upload directory.", 500);
    }

    if (empty($event->getFiles())) {
      throw new FilesException("Please choose at least one file to upload.", 500);
    }
  }

  /**
   * Compressed upload event handler.
   *
   * @param \Drupal\zipload\Event\ZiploadEvent $event
   *   Compressed upload event.
   *
   * @throws \Drupal\zipload\Exception\DestinationException
   * @throws \Drupal\zipload\Exception\FilesException
   */
  public function onCompressedUpload(ZiploadEvent $event) {
    if (empty($event->getDestination())) {
      throw new DestinationException("Please specify the upload directory.", 500);
    }

    if (substr($event->getData(), 0, 2) !== 'PK') {
      throw new FilesException("Upload failed because of wrong compression", 400);
    }
  }

}
