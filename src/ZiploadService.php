<?php

namespace Drupal\zipload;

use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;

/**
 * Zip upload service.
 */
class ZiploadService {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The path to upload directory.
   *
   * @var string
   */
  private $uploadDirectory;

  /**
   * Constructs a ZiploadService object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
  }

  /**
   * Set upload directory.
   *
   * @param string $destination
   *   The destination path.
   *
   * @return $this
   *
   * @throws \Exception
   */
  public function setUploadDirectory($destination) {
    if (strpos($destination, "public://") === 0 || strpos($destination, "private://") === 0) {
      $destination = $this->fileSystem->realpath($destination);
    }
    $options = FileSystem::CREATE_DIRECTORY | FileSystem::MODIFY_PERMISSIONS;

    if ($this->fileSystem->prepareDirectory($destination, $options)) {
      $this->uploadDirectory = $destination;
    }
    else {
      throw new \Exception("Unable to create the upload directory, verify your server permissions", 403);
    }

    return $this;
  }

  /**
   * Get upload directory.
   *
   * @return mixed
   *   The upload directory.
   */
  public function getUploadDirectory() {
    return $this->uploadDirectory;
  }

  /**
   * Save.
   *
   * @param string $data
   *   A string containing the contents of the file.
   *
   * @return string
   *   The path of saved file.
   */
  public function save($data) {
    $path = $this->uploadDirectory . "/zipload.zip";
    return $this->fileSystem->saveData($data, $path, FileSystem::EXISTS_REPLACE);
  }

  /**
   * Extract zip file to upload directory.
   *
   * @param string $path
   *   The path of compressed file.
   *
   * @throws \Exception
   */
  public function extract($path) {
    $zip = new \ZipArchive();
    $code = $zip->open($path);
    if ($code === TRUE) {
      $zip->extractTo($this->getUploadDirectory());
      $zip->close();
    }
    else {
      throw new \Exception($this->archiveErrorMessage($code), 500);
    }

    $this->fileSystem->delete($path);
  }

  /**
   * Get archive error message.
   *
   * @param int $code
   *   The error code.
   *
   * @return string
   *   The error message.
   */
  public static function archiveErrorMessage($code) {
    switch ($code) {
      case \ZipArchive::ER_OK:
        return "No error";

      case \ZipArchive::ER_MULTIDISK:
        return "Multi-disk zip archives not supported";

      case \ZipArchive::ER_RENAME:
        return "Renaming temporary file failed";

      case \ZipArchive::ER_CLOSE:
        return "Closing zip archive failed";

      case \ZipArchive::ER_SEEK:
        return "Seek error";

      case \ZipArchive::ER_READ:
        return "Read error";

      case \ZipArchive::ER_WRITE:
        return "Write error";

      case \ZipArchive::ER_CRC:
        return "CRC error";

      case \ZipArchive::ER_ZIPCLOSED:
        return "Containing zip archive was closed";

      case \ZipArchive::ER_NOENT:
        return "No such file";

      case \ZipArchive::ER_EXISTS:
        return "File already exists";

      case \ZipArchive::ER_OPEN:
        return "Can't open file";

      case \ZipArchive::ER_TMPOPEN:
        return "Failure to create temporary file";

      case \ZipArchive::ER_ZLIB:
        return "Zlib error";

      case \ZipArchive::ER_MEMORY:
        return "Memory allocation failure";

      case \ZipArchive::ER_CHANGED:
        return "Entry has been changed";

      case \ZipArchive::ER_COMPNOTSUPP:
        return "Compression method not supported";

      case \ZipArchive::ER_EOF:
        return "Premature EOF";

      case \ZipArchive::ER_INVAL:
        return "Invalid argument";

      case \ZipArchive::ER_NOZIP:
        return "Not a zip archive";

      case \ZipArchive::ER_INTERNAL:
        return "Internal error";

      case \ZipArchive::ER_INCONS:
        return "Zip archive inconsistent";

      case \ZipArchive::ER_REMOVE:
        return "Can't remove file";

      case \ZipArchive::ER_DELETED:
        return "Entry has been deleted";

      default:
        return "An unknown error has occurred(" . intval($code) . ")";
    }
  }

}
